﻿using System.IO;

namespace SensorsDataAnalyser.UnitTest.MockData
{
    public static class LoggedData
    {
        public static string GetMockData(string fileName) {
            return File.ReadAllText($"./MockData/Files/{fileName}");
        }
    }
}
