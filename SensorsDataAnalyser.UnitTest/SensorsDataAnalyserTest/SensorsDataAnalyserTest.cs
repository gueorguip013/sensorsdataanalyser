﻿using SensorsDataAnalyser.Configurations;
using SensorsDataAnalyser.Exceptions;
using SensorsDataAnalyser.Sensors;
using SensorsDataAnalyser.UnitTest.MockData;
using Shouldly;
using System;
using System.Collections.Generic;
using Xunit;

namespace SensorsDataAnalyser.UnitTest.AnalisisSensorsDataTest
{    
    public class SensorsDataAnalyserTest
    {

        private const string basicFileResult = "{\r\n  \"temp-1\": \"precise\",\r\n  \"temp-2\": \"ultra precise\",\r\n  \"hum-1\": \"keep\",\r\n  \"hum-2\": \"discard\",\r\n  \"mon-1\": \"keep\",\r\n  \"mon-2\": \"discard\"\r\n}";
        private const string configFileResult = "{\r\n  \"temp-1\": \"precise\",\r\n  \"temp-2\": \"very precise\",\r\n  \"hum-1\": \"keep\",\r\n  \"hum-2\": \"discard\",\r\n  \"mon-1\": \"keep\",\r\n  \"mon-2\": \"discard\"\r\n}";

        [Fact]
        public void EvaluteBasicFile()
        {            
            Analyze
                .EvaluateLogFile(LoggedData.GetMockData("BasicSample1.log"))
                .ShouldBe(basicFileResult);
        }

        [Fact]
        public void EvaluteBasicFileWithCustomeConfiguration()
        {
            var customConfig = new AnalysisSensorsConfiguration();
            customConfig.Thermometer.UltraPreciseStdDev = 0.5m;
            customConfig.SensorLabels.Thermometer = "t";
            customConfig.SensorLabels.Monoxide = "m";
            customConfig.SensorLabels.Humidistat = "h";

            customConfig.FileParse
                .SetReferenceTypeOrder(new string[] { nameof(Humidistat), nameof(Thermometer), nameof(Monoxide) });
                
            Analyze
                .EvaluateLogFile(LoggedData.GetMockData("BasicSampleConfigChanges.log"), customConfig)
                .ShouldBe(configFileResult);
        }

        [Fact]
        public void EvaluteFileNotEndsWithCarriageReturn()
        {
            Analyze
                .EvaluateLogFile(LoggedData.GetMockData("NotEndsWithCarriageReturn.log"))
                .ShouldBe(basicFileResult);
        }

        [Fact]
        public void EvaluteFileWithSomeEmptyData()
        {
            Analyze
                .EvaluateLogFile(LoggedData.GetMockData("SomeEmptyData.log"))
                .ShouldBe(basicFileResult);
        }

        [Fact]
        public void EvaluteEmptyFile()
        {
            var ex = Should.Throw<Exception>(() => Analyze
                .EvaluateLogFile(LoggedData.GetMockData("Empty.log")));
            ex.Message.ShouldBe(CustomExeptionMessages.FileEmpty);            
        }

        [Fact]
        public void EvaluteFileWithNotRefence()
        {
            var ex = Should.Throw<Exception>(() => Analyze
                .EvaluateLogFile(LoggedData.GetMockData("WithNotReference.log")));
            ex.Message.ShouldBe(CustomExeptionMessages.ReferenceNotPresent);
        }

        [Fact]
        public void EvaluteFileWithSensorsWithNotData()
        {
            Analyze
                .EvaluateLogFile(LoggedData.GetMockData("SensorsNotData.log"))
                .ShouldBe("{\r\n  \"temp-1\": \"\",\r\n  \"temp-2\": \"\",\r\n  \"hum-1\": \"\",\r\n  \"hum-2\": \"\",\r\n  \"mon-1\": \"\",\r\n  \"mon-2\": \"\"\r\n}");
        }

        [Fact]
        public void EvaluteInvalidDatesInFile()
        {
            var ex = Should.Throw<Exception>(() => Analyze
                .EvaluateLogFile(LoggedData.GetMockData("InvalidData1.log")));
            ex.Message.ShouldBe(CustomExeptionMessages.InvalidFile);            
        }

        [Fact]
        public void EvaluteMissingReferenceValues()
        {
            var ex = Should.Throw<Exception>(() => Analyze
                .EvaluateLogFile(LoggedData.GetMockData("InvalidData2.log")));
            ex.Message.ShouldBe(string.Format(CustomExeptionMessages.ReferenceValueMissing, nameof(Monoxide)));
        }

        [Fact]
        public void EvaluteMissingReadingValue()
        {
            var ex = Should.Throw<Exception>(() => Analyze
                .EvaluateLogFile(LoggedData.GetMockData("MissingReadsValue.log")));
            ex.Message.ShouldBe(CustomExeptionMessages.UnexpectedErrorParsingFile);
        }
    }
}


