﻿using SensorsDataAnalyser.Sensors;
using System;
using Shouldly;
using Xunit;
using SensorsDataAnalyser.Configurations;
using SensorsDataAnalyser.Enums;

namespace SensorsDataAnalyser.UnitTest.SensorsTests
{
    public class MonoxideTest
    {
        [Fact]
        public void CheckKeep()
        {
            var monoxide = new Monoxide(new AnalysisSensorsConfiguration());
            var config = new AnalysisSensorsConfiguration();
            var referenceValue = 55;

            monoxide.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 57.5m });
            monoxide.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 58.0m });
            monoxide.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 55.5m });
            monoxide.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 54.0m });
            monoxide.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 53.9m });
            monoxide.GetEvaluation(referenceValue)
                .ShouldBe(config.Monoxide.ResultLevels[(int)MonoxideLevelsEnum.Keep]);
        }


        [Fact]
        public void CheckDiscard()
        {
            var monoxide = new Monoxide(new AnalysisSensorsConfiguration());
            var config = new AnalysisSensorsConfiguration();
            var referenceValue = 55;

            monoxide.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 57.5m });
            monoxide.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 58.1m });
            monoxide.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 55.5m });
            monoxide.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 54.5m });
            monoxide.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 53.9m });
            monoxide.GetEvaluation(referenceValue)
                .ShouldBe(config.Monoxide.ResultLevels[(int)MonoxideLevelsEnum.Discard]);
        }
    }
}
