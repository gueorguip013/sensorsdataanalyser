﻿using SensorsDataAnalyser.Sensors;
using System;
using Shouldly;
using Xunit;
using SensorsDataAnalyser.Configurations;
using SensorsDataAnalyser.Enums;

namespace SensorsDataAnalyser.UnitTest.SensorsTests
{
    public class HumidistatTest
    {
        [Fact]
        public void CheckKeep()
        {
            var humidistat = new Humidistat(new AnalysisSensorsConfiguration());
            var config = new AnalysisSensorsConfiguration();
            var referenceValue = 55;

            humidistat.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 56.0m });
            humidistat.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 55.5m });
            humidistat.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 54.5m });
            humidistat.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 54.0m });
            humidistat.GetEvaluation(referenceValue)
                .ShouldBe(config.Monoxide.ResultLevels[(int)HumidistatLevelsEnum.Keep]);
        }


        [Fact]
        public void CheckDiscard()
        {
            var humidistat = new Humidistat(new AnalysisSensorsConfiguration());
            var config = new AnalysisSensorsConfiguration();
            var referenceValue = 55;

            humidistat.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 56.5m });
            humidistat.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 55.5m });
            humidistat.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 54.5m });
            humidistat.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 40.9m });
            humidistat.GetEvaluation(referenceValue)
                .ShouldBe(config.Monoxide.ResultLevels[(int)HumidistatLevelsEnum.Discard]);
        }
    }
}
