﻿using SensorsDataAnalyser.Sensors;
using System;
using Shouldly;
using Xunit;
using SensorsDataAnalyser.Configurations;
using SensorsDataAnalyser.Enums;

namespace SensorsDataAnalyser.UnitTest.SensorsTests
{    
    public class ThermometerTest
    {
        [Fact]
        public void CheckUltraPrecise()
        {
            var thermometer = new Thermometer(new AnalysisSensorsConfiguration());
            var config = new AnalysisSensorsConfiguration();
            var referenceValue = 80;

            thermometer.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 80 });
            thermometer.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 79 });
            thermometer.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 81 });
            thermometer.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 79.5m });            
            thermometer.GetEvaluation(referenceValue)
                .ShouldBe(config.Thermometer.ResultLevels[(int)ThermometerLevelsEnum.UltraPrecise]);
        }

        [Fact]
        public void CheckVeryPrecise()
        {
            var thermometer = new Thermometer(new AnalysisSensorsConfiguration());
            var config = new AnalysisSensorsConfiguration();
            var referenceValue = 80;

            thermometer.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 82.1m });
            thermometer.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 83.9m });
            thermometer.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 76.1m });
            thermometer.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 77.9m });
            thermometer.GetEvaluation(referenceValue)
                .ShouldBe(config.Thermometer.ResultLevels[(int)ThermometerLevelsEnum.VeryPrecise]);
        }

        [Fact]
        public void CheckPrecise()
        {
            var thermometer = new Thermometer(new AnalysisSensorsConfiguration());
            var config = new AnalysisSensorsConfiguration();
            var referenceValue = 80;

            thermometer.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 85.1m });
            thermometer.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 85.9m });
            thermometer.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 76.1m });
            thermometer.Measures.Add(new Helpers.Measure { Date = DateTime.Now, Value = 75.9m });
            thermometer.GetEvaluation(referenceValue)
                .ShouldBe(config.Thermometer.ResultLevels[(int)ThermometerLevelsEnum.Precise]);
        }
    }
}
