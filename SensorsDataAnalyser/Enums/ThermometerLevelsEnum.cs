﻿namespace SensorsDataAnalyser.Enums
{
    public enum ThermometerLevelsEnum
    {
        Precise = 0,
        VeryPrecise,
        UltraPrecise                
    }
}
