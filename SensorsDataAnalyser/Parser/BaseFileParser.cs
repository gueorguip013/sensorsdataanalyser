﻿using SensorsDataAnalyser.Helpers;
using System.Collections.Generic;

namespace SensorsDataAnalyser.Parser
{
    internal abstract class BaseFileParser: Queue<string>
    {
        public abstract SensorsData Parse(string fileContent);
    }
}
