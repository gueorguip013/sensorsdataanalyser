﻿using SensorsDataAnalyser.Configurations;
using SensorsDataAnalyser.Exceptions;
using SensorsDataAnalyser.Helpers;
using SensorsDataAnalyser.Sensors;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SensorsDataAnalyser.Parser
{
    internal class FileParser : BaseFileParser
    {
        private readonly AnalysisSensorsConfiguration config;
        private readonly SensorFactory sensorFactory;

        public FileParser(SensorFactory sensorFactory, AnalysisSensorsConfiguration config = null)
        {
            this.config = config ?? new AnalysisSensorsConfiguration();
            this.sensorFactory = sensorFactory;
        }

        private IDictionary<string, decimal> GetReferenceData(string[] referenceLineRaw)
        {
            var references = new Dictionary<string, decimal>();

            if (referenceLineRaw.Length == 0
                || string.Compare(referenceLineRaw[0], config.FileParse.ReferenceLabel, StringComparison.OrdinalIgnoreCase) != 0)
                    throw new CustomException(CustomExeptionMessages.ReferenceNotPresent);
            
            for (int index =1; index < referenceLineRaw.Length; index++)
            {
                references.Add(config.FileParse.ReferenceTypeValuesOrder[index-1], decimal.Parse(referenceLineRaw[index]));
            }                
            
            return references;
        }

        private string[] DequeueLineSplited() => Dequeue().Split(config.FileParse.ColumnSeparator);

        private void EnsureSensorObject(ISensor sensor, string[] line) => 
            _ = sensor ?? throw new CustomException(CustomExeptionMessages.InvalidFile, 
                new CustomException($"Error trying to create a object name {line[0]}, " +
                    $"from line: {string.Join(config.FileParse.ColumnSeparator, line)}"));

        private void ParseSensorsMeasures(string[] sensorTypeRawLine, SensorsData sensorsData) 
        {
            if (string.IsNullOrEmpty(sensorTypeRawLine[0]) || string.IsNullOrEmpty(sensorTypeRawLine[1])) return;
            
            var sensor = sensorFactory.CreateObject(sensorTypeRawLine[0]);

            EnsureSensorObject(sensor, sensorTypeRawLine);
            
            sensor.Name = sensorTypeRawLine[1];

            var measureRawData = DequeueLineSplited();
                        
            while (DateTime.TryParse(measureRawData[0], out DateTime result)) {
                sensor.Measures.Add(new Measure
                {
                    Date = result,
                    Value = decimal.Parse(measureRawData[1])
                });

                measureRawData = DequeueLineSplited();
            }

            sensorsData.Sensors.Add(sensor);

            if (Count > 0) ParseSensorsMeasures(measureRawData, sensorsData);
        }

        private void PushLineToQueue(string fileContent) 
        {               
            fileContent.Split(Environment.NewLine)
                .Where(l => !string.IsNullOrEmpty(l))
                .ToList()
                .ForEach(l => Enqueue(l));

            //Add new line at the end.
            Enqueue(Environment.NewLine);
        }

        private void EnsureLastNewLine(ref string fileContent) {
            if (!fileContent.EndsWith(Environment.NewLine)) fileContent = string.Concat(fileContent, Environment.NewLine);
        }

        private void EnsureFileIsNotEmpty(string fileContent)
        {
            if (fileContent.Length == 0) throw new CustomException(CustomExeptionMessages.FileEmpty);
        }

        public override SensorsData Parse(string fileContent)
        {
            try
            {
                EnsureFileIsNotEmpty(fileContent);
                EnsureLastNewLine(ref fileContent);

                var sensorsData = new SensorsData();

                PushLineToQueue(fileContent);
                sensorsData.References = GetReferenceData(DequeueLineSplited());
                ParseSensorsMeasures(DequeueLineSplited(), sensorsData);

                return sensorsData;
            }
            catch (CustomException) 
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new CustomException(CustomExeptionMessages.UnexpectedErrorParsingFile, ex);
            }
            
        }
    }
}
