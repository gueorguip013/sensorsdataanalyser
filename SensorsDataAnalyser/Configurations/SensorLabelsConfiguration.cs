﻿namespace SensorsDataAnalyser.Configurations
{
    public class SensorLabelsConfiguration
    {
        public string Thermometer { get; set; } = "thermometer";
        public string Humidistat { get; set; } = "humidity";
        public string Monoxide { get; set; } = "monoxide";
    }
}
