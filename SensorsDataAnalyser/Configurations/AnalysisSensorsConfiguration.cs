﻿using SensorsDataAnalyser.Configurations.Sensors;

namespace SensorsDataAnalyser.Configurations
{
    public class AnalysisSensorsConfiguration
    {
        public FileParseConfiguration FileParse { get; set; }
        public SensorLabelsConfiguration SensorLabels { get; set; }
        public ThermometerConfiguration Thermometer { get; set; }
        public MonoxideConfiguration Monoxide { get; set; }
        public HumidistatConfiguration Humidistat { get; set; }
        
        public AnalysisSensorsConfiguration()
        {
            FileParse = new FileParseConfiguration();
            SensorLabels = new SensorLabelsConfiguration();
            Thermometer = new ThermometerConfiguration();
            Monoxide = new MonoxideConfiguration();
            Humidistat = new HumidistatConfiguration();
        }

    }
}
