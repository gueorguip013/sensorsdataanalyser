﻿using SensorsDataAnalyser.Enums;
using System.Collections.Generic;

namespace SensorsDataAnalyser.Configurations.Sensors
{
    public class HumidistatConfiguration
    {
        public decimal ProximityLimit { get; set; } = 1;

        public Dictionary<int, string> ResultLevels { get; } = new Dictionary<int, string>
        {
            { (int)HumidistatLevelsEnum.Keep , "keep" },
            { (int)HumidistatLevelsEnum.Discard , "discard" }
        };
    }
}
