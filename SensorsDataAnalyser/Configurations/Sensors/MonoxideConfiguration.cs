﻿using SensorsDataAnalyser.Enums;
using System.Collections.Generic;

namespace SensorsDataAnalyser.Configurations.Sensors
{
    public class MonoxideConfiguration
    {
        public decimal ProximityLimit { get; set; } = 3;

        public Dictionary<int, string> ResultLevels { get; } = new Dictionary<int, string>
        {
            { (int)MonoxideLevelsEnum.Keep , "keep" },
            { (int)MonoxideLevelsEnum.Discard , "discard" }
        };        
    }
}
