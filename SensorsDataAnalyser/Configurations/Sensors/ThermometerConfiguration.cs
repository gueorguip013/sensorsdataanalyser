﻿using SensorsDataAnalyser.Enums;
using System.Collections.Generic;

namespace SensorsDataAnalyser.Configurations.Sensors
{
    public class ThermometerConfiguration
    {
        public decimal UltraPreciseStdDev { get; set; } = 3;
        public decimal VeryPreciseStdDev { get; set; } = 5;
        public decimal ProximityLimit { get; set; } = 0.5m;
        public Dictionary<int,string> ResultLevels { get; } = new Dictionary<int, string> 
        {
            { (int)ThermometerLevelsEnum.Precise , "precise" },
            { (int)ThermometerLevelsEnum.UltraPrecise , "ultra precise" },
            { (int)ThermometerLevelsEnum.VeryPrecise , "very precise" }
        };
    }
}
