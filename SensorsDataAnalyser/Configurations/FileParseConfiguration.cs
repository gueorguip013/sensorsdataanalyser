﻿using SensorsDataAnalyser.Sensors;
using System.Collections.Generic;

namespace SensorsDataAnalyser.Configurations
{
    public class FileParseConfiguration
    {
        public string ReferenceLabel { get; set; } = "reference";        
        public List<string> ReferenceTypeValuesOrder { get; } = 
            new List<string> { nameof(Thermometer), nameof(Humidistat), nameof(Monoxide) };        
        public string ColumnSeparator { get; set; } = " ";

        public void SetReferenceTypeOrder(string [] referencesTypes) {
            ReferenceTypeValuesOrder.Clear();
            ReferenceTypeValuesOrder.AddRange(referencesTypes);
        }

    }
}
