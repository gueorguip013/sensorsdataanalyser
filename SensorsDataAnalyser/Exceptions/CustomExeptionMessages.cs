﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("SensorsDataAnalyser.UnitTest")]
namespace SensorsDataAnalyser.Exceptions
{
    internal static class CustomExeptionMessages
    {
        public const string FileEmpty = "File is empty.";
        public const string ReferenceNotPresent = "Reference values not found.";
        public const string InvalidFile = "Invalid file.";
        public const string ReferenceValueMissing = "Missing reference value for {0}.";
        public const string UnexpectedErrorParsingFile = "An unexpected error ocurrs trying to parse the file. " +
            "Please check the inner exception for more information.";
        public const string UnexpectedError = "An unexpected error ocurrs. " +
            "Please check the inner exception for more information.";
    }
}
