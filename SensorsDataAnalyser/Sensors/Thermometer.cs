﻿using SensorsDataAnalyser.Configurations;
using SensorsDataAnalyser.Sensors.Rules.ThermometerRules;
using System;
using System.Collections.Generic;

namespace SensorsDataAnalyser.Sensors
{
    internal class Thermometer : BaseSensor
    {
        private readonly List<IThermometerRules> sensorRules;
        
        public Thermometer(AnalysisSensorsConfiguration config): base(config)
        {
            sensorRules = new List<IThermometerRules> {
                { new PreciseRule(config.Thermometer) },
                { new UltraPreciseRule(config.Thermometer) },
                { new VeryPreciseRule(config.Thermometer) }
            };                        
        }
                
        public override string GetEvaluation(decimal referenceValue)
        {
            if (Measures.Count == 0) return string.Empty;

            var level = -1;
            var average = GetAverage();
            var stdDev = StdDev(average);
            var proximity = Math.Abs(referenceValue - average);

            sensorRules.ForEach(s => level = Math.Max(s.CalculateSensorLevel(proximity, stdDev), level));
            
            return Config.Thermometer.ResultLevels.TryGetValue(level, out string levelText) ? levelText : string.Empty;
            
        }
    }
}
