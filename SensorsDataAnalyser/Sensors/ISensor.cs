﻿using SensorsDataAnalyser.Helpers;
using System.Collections.Generic;

namespace SensorsDataAnalyser.Sensors
{
    internal interface ISensor
    {
        string Name { get; set; }
        List<Measure> Measures { get; set; }
        string GetEvaluation(decimal referenceValue);
    }
}
