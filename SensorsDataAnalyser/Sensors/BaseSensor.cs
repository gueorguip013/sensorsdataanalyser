﻿using SensorsDataAnalyser.Helpers;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Linq;
using System;
using SensorsDataAnalyser.Configurations;

[assembly: InternalsVisibleTo("SensorsDataAnalyser.UnitTest")]
namespace SensorsDataAnalyser.Sensors
{
    internal abstract class BaseSensor : ISensor
    {                
        public string Name { get; set; }
        public List<Measure> Measures { get; set; }        
        public abstract string GetEvaluation(decimal referenceValue);        
        protected AnalysisSensorsConfiguration Config { get; set; }
        public BaseSensor(AnalysisSensorsConfiguration config)
        {
            Measures = new List<Measure>();
            Config = config;
        }
        
        protected decimal StdDev(decimal average) 
        {
            if (Measures.Count == 0) return 0;

            double sum = Measures.Sum(s => Math.Pow(Convert.ToDouble(s.Value - average), 2));

            return Convert.ToDecimal(Math.Sqrt(sum/Convert.ToDouble(Measures.Count)));
            
        }

        protected decimal GetAverage()
        {
            return Measures.Count > 0 ? Measures.Average(s => s.Value) : 0;
        }

    }
}
