﻿using SensorsDataAnalyser.Configurations;
using SensorsDataAnalyser.Sensors.Rules.HumidistatRules;
using System;
using System.Collections.Generic;

namespace SensorsDataAnalyser.Sensors
{
    internal class Humidistat : BaseSensor
    {
        private readonly List<IHumidistatRule> sensorRules;
        public Humidistat(AnalysisSensorsConfiguration config) : base(config)
        {
            sensorRules = new List<IHumidistatRule> {
                { new KeepRule(config.Humidistat) },
                { new DiscardRule(config.Humidistat) }
            };
        }
        
        public override string GetEvaluation(decimal referenceValue)
        {
            if (Measures.Count == 0) return string.Empty;

            var level = -1;
            
            sensorRules.ForEach(s => level = Math.Max(s.CalculateSensorLevel(Measures, referenceValue), level));
            
            return Config.Humidistat.ResultLevels.TryGetValue(level, out string levelText) ? levelText : string.Empty;
        }
    }
}
