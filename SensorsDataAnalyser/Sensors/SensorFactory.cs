﻿using SensorsDataAnalyser.Configurations;
using System;
using System.Collections.Generic;

namespace SensorsDataAnalyser.Sensors
{
    internal class SensorFactory
    {
        private readonly AnalysisSensorsConfiguration config;
        private readonly Dictionary<string, Func<ISensor>> sensorFactories;

        public SensorFactory(AnalysisSensorsConfiguration config = null)
        {
            this.config = config ?? new AnalysisSensorsConfiguration();
            sensorFactories = new Dictionary<string, Func<ISensor>>
            {
                { this.config.SensorLabels.Thermometer, ()=> new Thermometer(this.config) },
                { this.config.SensorLabels.Humidistat, ()=> new Humidistat(this.config) },
                { this.config.SensorLabels.Monoxide, ()=> new Monoxide(this.config) },
            };
        }
      
        public ISensor CreateObject(string type) {
            var factory = sensorFactories.TryGetValue(type,out var sensorObject) ? sensorObject : null;
            return factory == null ? null : factory();
        }
    }
}
