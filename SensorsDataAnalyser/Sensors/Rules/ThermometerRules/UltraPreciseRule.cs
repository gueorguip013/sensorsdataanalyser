﻿using SensorsDataAnalyser.Configurations.Sensors;
using SensorsDataAnalyser.Enums;

namespace SensorsDataAnalyser.Sensors.Rules.ThermometerRules
{
    internal class UltraPreciseRule : IThermometerRules
    {
        private readonly ThermometerConfiguration config;
        public UltraPreciseRule(ThermometerConfiguration config)
        {
            this.config = config;
        }
        public int CalculateSensorLevel(decimal proximity, decimal stdDev) {

            if (proximity <= config.ProximityLimit && stdDev < config.UltraPreciseStdDev) {
                return (int)ThermometerLevelsEnum.UltraPrecise;
            }

            return -1;
        }            
    }
}
