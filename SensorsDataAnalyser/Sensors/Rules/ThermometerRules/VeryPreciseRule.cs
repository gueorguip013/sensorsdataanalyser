﻿using SensorsDataAnalyser.Configurations.Sensors;
using SensorsDataAnalyser.Enums;

namespace SensorsDataAnalyser.Sensors.Rules.ThermometerRules
{
    internal class VeryPreciseRule : IThermometerRules
    {
        private readonly ThermometerConfiguration config;
        public VeryPreciseRule(ThermometerConfiguration config)
        {
            this.config = config;
        }
        public int CalculateSensorLevel(decimal proximity, decimal stdDev) {

            if (proximity <= config.ProximityLimit && stdDev < config.VeryPreciseStdDev) {
                return (int)ThermometerLevelsEnum.VeryPrecise;
            }

            return -1;
        }            
    }
}
