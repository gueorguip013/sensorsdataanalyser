﻿namespace SensorsDataAnalyser.Sensors.Rules.ThermometerRules
{
    internal interface IThermometerRules
    {
        int CalculateSensorLevel(decimal proximity, decimal stdDev);
    }
}
