﻿using SensorsDataAnalyser.Configurations.Sensors;
using SensorsDataAnalyser.Enums;

namespace SensorsDataAnalyser.Sensors.Rules.ThermometerRules
{
    internal class PreciseRule : IThermometerRules
    {        
        public PreciseRule(ThermometerConfiguration _)
        {     
        }
        public int CalculateSensorLevel(decimal _, decimal __) => (int)ThermometerLevelsEnum.Precise;                     
    }
}
