﻿using SensorsDataAnalyser.Helpers;
using System.Collections.Generic;

namespace SensorsDataAnalyser.Sensors.Rules.MonoxideRules
{
    internal interface IMonoxideRule
    {
        int CalculateSensorLevel(List<Measure> measures, decimal referenceValue);
    }
}
