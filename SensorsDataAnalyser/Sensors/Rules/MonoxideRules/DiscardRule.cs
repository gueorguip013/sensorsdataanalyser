﻿using SensorsDataAnalyser.Configurations.Sensors;
using SensorsDataAnalyser.Enums;
using SensorsDataAnalyser.Helpers;
using System.Collections.Generic;

namespace SensorsDataAnalyser.Sensors.Rules.MonoxideRules
{
    internal class DiscardRule : IMonoxideRule
    {        
        public DiscardRule(MonoxideConfiguration _)
        {

        }
        public int CalculateSensorLevel(List<Measure> _, decimal __) => (int)MonoxideLevelsEnum.Discard;        
    }
}
