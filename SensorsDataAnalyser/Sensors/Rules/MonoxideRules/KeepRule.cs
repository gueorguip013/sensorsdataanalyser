﻿using SensorsDataAnalyser.Configurations.Sensors;
using SensorsDataAnalyser.Enums;
using SensorsDataAnalyser.Helpers;
using System;
using System.Collections.Generic;

namespace SensorsDataAnalyser.Sensors.Rules.MonoxideRules
{
    internal class KeepRule : IMonoxideRule
    {
        private readonly MonoxideConfiguration config;
        public KeepRule(MonoxideConfiguration config)
        {
            this.config = config;
        }
        public int CalculateSensorLevel(List<Measure> measures, decimal referenceValue)
        {            
            var isInLimits = true;
            foreach (var measure in measures)
            {
                if (Math.Abs(referenceValue - measure.Value) > config.ProximityLimit) {
                    isInLimits = false;
                    break;
                }
            }

            if (isInLimits) return (int)MonoxideLevelsEnum.Keep;

            return -1;
        }
    }
}
