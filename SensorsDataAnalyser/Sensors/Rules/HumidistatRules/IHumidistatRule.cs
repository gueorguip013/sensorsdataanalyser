﻿using SensorsDataAnalyser.Helpers;
using System.Collections.Generic;

namespace SensorsDataAnalyser.Sensors.Rules.HumidistatRules
{
    internal interface IHumidistatRule
    {
        int CalculateSensorLevel(List<Measure> measures, decimal referenceValue);
    }
}
