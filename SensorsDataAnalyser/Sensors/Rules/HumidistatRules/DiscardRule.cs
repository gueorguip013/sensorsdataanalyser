﻿using SensorsDataAnalyser.Configurations.Sensors;
using SensorsDataAnalyser.Enums;
using SensorsDataAnalyser.Helpers;
using System.Collections.Generic;

namespace SensorsDataAnalyser.Sensors.Rules.HumidistatRules
{
    internal class DiscardRule : IHumidistatRule
    {        
        public DiscardRule(HumidistatConfiguration _)
        {

        }
        public int CalculateSensorLevel(List<Measure> _, decimal __) => (int)HumidistatLevelsEnum.Discard;
       
    }
}
