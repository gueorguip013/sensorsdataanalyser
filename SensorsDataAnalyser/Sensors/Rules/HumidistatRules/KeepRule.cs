﻿using SensorsDataAnalyser.Configurations.Sensors;
using SensorsDataAnalyser.Enums;
using SensorsDataAnalyser.Helpers;
using System;
using System.Collections.Generic;

namespace SensorsDataAnalyser.Sensors.Rules.HumidistatRules
{
    internal class KeepRule : IHumidistatRule
    {
        private readonly HumidistatConfiguration config;
        public KeepRule(HumidistatConfiguration config)
        {
            this.config = config;
        }
        public int CalculateSensorLevel(List<Measure> measures, decimal referenceValue)
        {
            var isInLimits = true;
            foreach (var measure in measures)
            {
                if (Math.Abs(referenceValue - measure.Value) > config.ProximityLimit)
                {
                    isInLimits = false;
                    break;
                }
            }

            if (isInLimits) return (int)MonoxideLevelsEnum.Keep;

            return -1;           
        }
    }
}
