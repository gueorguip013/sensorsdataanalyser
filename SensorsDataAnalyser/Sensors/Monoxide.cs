﻿using SensorsDataAnalyser.Configurations;
using SensorsDataAnalyser.Sensors.Rules.MonoxideRules;
using System;
using System.Collections.Generic;

namespace SensorsDataAnalyser.Sensors
{
    internal class Monoxide : BaseSensor
    {
        private readonly List<IMonoxideRule> sensorRules;
        public Monoxide(AnalysisSensorsConfiguration config) : base(config)
        {
            sensorRules = new List<IMonoxideRule> {
                { new KeepRule(config.Monoxide) },
                { new DiscardRule(config.Monoxide) }
            };

        }
        
        public override string GetEvaluation(decimal referenceValue)
        {
            if (Measures.Count == 0) return string.Empty;

            var level = -1;
                        
            sensorRules.ForEach(s => level = Math.Max(s.CalculateSensorLevel(Measures,referenceValue), level));
            
            return Config.Monoxide.ResultLevels.TryGetValue(level, out string levelText) ? levelText : string.Empty;            
        }
    }
}
