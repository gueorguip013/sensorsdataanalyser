﻿using SensorsDataAnalyser.Parser;
using SensorsDataAnalyser.Helpers;
using SensorsDataAnalyser.Sensors;
using SensorsDataAnalyser.Exceptions;
using System;
using SensorsDataAnalyser.Configurations;

namespace SensorsDataAnalyser
{
    public static class Analyze
    {
        public static string EvaluateLogFile(string logContentsStr, AnalysisSensorsConfiguration config = null)
        {
            try
            {
                var fileParser = new FileParser(new SensorFactory(config), config);
                var sensorsData = fileParser.Parse(logContentsStr);

                return ResultParser.Parse(sensorsData.CalculateValues());
            }
            catch (CustomException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new Exception(CustomExeptionMessages.UnexpectedError, ex);
            }
            
        }        
    }
}
