﻿namespace SensorsDataAnalyser.Helpers
{
    internal class AnalysisResult
    {
        public string SensorName { get; set; }
        public string Value { get; set; }

    }
}
