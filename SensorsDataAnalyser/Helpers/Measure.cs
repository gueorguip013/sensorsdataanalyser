﻿using System;

namespace SensorsDataAnalyser.Helpers
{
    internal class Measure
    {
        public DateTime Date { get; set; }
        public decimal Value { get; set; }
    }
}
