﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;

namespace SensorsDataAnalyser.Helpers
{
    internal static class ResultParser
    {
        public static string Parse(IEnumerable<AnalysisResult> analysisResult) => 
            new JObject(analysisResult.Select(s => new JProperty(s.SensorName, s.Value))).ToString();        
    }
}
