﻿using SensorsDataAnalyser.Exceptions;
using SensorsDataAnalyser.Sensors;
using System.Collections.Generic;

namespace SensorsDataAnalyser.Helpers
{
    internal class SensorsData
    {
        public IDictionary<string,decimal> References { get; set; }       
        public List<ISensor> Sensors { get; set; }

        public SensorsData()
        {
            References = new Dictionary<string, decimal>();
            Sensors = new List<ISensor>();
        }

        public IEnumerable<AnalysisResult> CalculateValues()
        {
            var result = new List<AnalysisResult>();
            foreach (var sensor in Sensors)
            {
                if (References.TryGetValue(sensor.GetType().Name, out decimal referenceValue))
                {
                    result.Add(new AnalysisResult
                    {
                        SensorName = sensor.Name,
                        Value = sensor.GetEvaluation(referenceValue)
                    });
                }
                else 
                {
                    throw new CustomException(
                        string.Format(CustomExeptionMessages.ReferenceValueMissing,sensor.GetType().Name));
                }
            }

            return result;
        }
    }
}
